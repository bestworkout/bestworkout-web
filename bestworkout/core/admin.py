from django.contrib import admin
from models import Company, City

class CompanyAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'title', 'email', 'phone1_ddd', 'phone1', 'active', 'featured')
    #date_hierarchy = 'created'
    search_fields = ('title', 'email')


# Register your models here.
admin.site.register(Company, CompanyAdmin)
admin.site.register(City)
